import java.util.Scanner;

public class NEWXO1 {
        
	public static String player = "O";
	public static int roundturn = 0;
        public static String[][] table = { { "_", "_", "_" },
                                           { "_", "_", "_" }, 
                                           { "_", "_", "_" } };

	public static void showWelcome() {
		System.out.println("Welcome to OX Game");
	}
        
	public static void showTable() {
		for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table.length; j++) {
				System.out.print(table[i][j] + " ");
			}
			System.out.println("");
		}
               System.out.println(""); 
	}
        
	public static void showTurn() {
            System.out.println("Turn "+player);
        }
        
	public static void switchPlayer() {
               if(player.equals("X")){
                   player="O";
               }else{
                   player="X";
               }
	}
        
	public static void inputRowCol() {
                int Row;
                int Col;
                Scanner kb = new Scanner(System.in);
		System.out.println("Please input Row Col : ");
		Row = kb.nextInt() - 1;
		Col = kb.nextInt() - 1;
		table[Row][Col] = player;
	}

	public static boolean checkWin() {
		if (checkCol() == true) {
			return true;
		} else if (checkRow() == true) {
			return true;
		} else if (checkOblique1() == true) {
			return true;
		} else if (checkOblique2() == true) {
			return true;
		} else if (checkDraw() == true) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean checkCol() {
		for (int i = 0; i < table.length; i++) {
			if (table[0][i].equals(player) && 
					table[1][i].equals(player) && 
					table[2][i].equals(player)) {
				return true;
			}
		}
		return false;
	}

	public static boolean checkRow() {
		for (int i = 0; i < table.length; i++) {
			if (table[i][0].equals(player) && 
					table[i][1].equals(player) && 
					table[i][2].equals(player)) {
				return true;
			}
		}
		return false;
	}

	public static boolean checkOblique1() {
		if (table[0][0].equals(player) && 
				table[1][1].equals(player) && 
				table[2][2].equals(player)) {
			return true;
		}
		return false;
	}

	public static boolean checkOblique2() {
		if (table[0][2].equals(player) && 
				table[1][1].equals(player) && 
				table[2][0].equals(player)) {
			return true;
		}
		return false;
	}

	public static boolean checkDraw() {
		if (roundturn > 8) {
			return true;
		}
		return false;
	}

	public static void showWin() {
		System.out.println("Player " + player + " Win!!");
	}

	public static void showDraw() {
		System.out.println("Draw!!");
	}

	public static void main(String[] args) {
		showWelcome();
		showTable();
		while (checkWin() != true) {
                        switchPlayer();
			showTurn();
			inputRowCol();
			showTable();
                        roundturn++;
		}
		if (checkDraw() == true) {
			showDraw();
		} else {
			showWin();
		}
	}

    