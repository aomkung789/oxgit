import java.util.Scanner;

public class XO {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		int row = 3, col = 3;
		int xwin = 0;
		int owin = 0;
		int Row, Col;
		int count = 0;
		String charac;
		String[][] xo = { { "_", "_", "_" }, 
                                  { "_", "_", "_" }, 
                                  { "_", "_", "_" } };
		System.out.println("Welcome to game OX");
		System.out.println("------------------");
		System.out.println("_ _ _");
		System.out.println("_ _ _");
		System.out.println("_ _ _");
		System.out.println();
		while (count < 9) {
			if(xwin != 0 || owin != 0)
				break;
			System.out.println("Turn x or o ?(input x/o)");
			charac = kb.next();
			System.out.print("Please in put row, col(Please enter only 1-3) : ");
			Row = kb.nextInt() - 1;
			Col = kb.nextInt() - 1;
			xo[Row][Col] = charac;
			for (int i = 0; i < row; i++) {
				for (int j = 0; j < col; j++) {
					System.out.print(" " + xo[i][j]);
				}
				System.out.println();
			}
			if (charac.equals("x")) {
				if(xo[0][0].equals("x")&&xo[0][1].equals("x")&&xo[0][2].equals("x"))
					xwin++;
				if(xo[1][0].equals("x")&&xo[1][1].equals("x")&&xo[1][2].equals("x"))
					xwin++;
				if(xo[2][0].equals("x")&&xo[2][1].equals("x")&&xo[2][2].equals("x"))
					xwin++;
				if(xo[0][0].equals("x")&&xo[1][0].equals("x")&&xo[2][0].equals("x"))
					xwin++;
				if(xo[0][1].equals("x")&&xo[1][1].equals("x")&&xo[2][1].equals("x"))
					xwin++;
				if(xo[0][2].equals("x")&&xo[1][2].equals("x")&&xo[2][2].equals("x"))
					xwin++;
				if(xo[0][0].equals("x")&&xo[1][1].equals("x")&&xo[2][2].equals("x"))
					xwin++;
				if(xo[0][2].equals("x")&&xo[1][1].equals("x")&&xo[2][0].equals("x"))
					xwin++;
			}else if(charac.equals("o")) {
				if(xo[0][0].equals("o")&&xo[0][1].equals("o")&&xo[0][2].equals("o"))
					owin++;
				if(xo[1][0].equals("o")&&xo[1][1].equals("o")&&xo[1][2].equals("o"))
					owin++;
				if(xo[2][0].equals("o")&&xo[2][1].equals("o")&&xo[2][2].equals("o"))
					owin++;
				if(xo[0][0].equals("o")&&xo[1][0].equals("o")&&xo[2][0].equals("o"))
					owin++;
				if(xo[0][1].equals("o")&&xo[1][1].equals("o")&&xo[2][1].equals("o"))
					owin++;
				if(xo[0][2].equals("o")&&xo[1][2].equals("o")&&xo[2][2].equals("o"))
					owin++;
				if(xo[0][0].equals("o")&&xo[1][1].equals("o")&&xo[2][2].equals("o"))
					owin++;
				if(xo[0][2].equals("o")&&xo[1][1].equals("o")&&xo[2][0].equals("o"))
					owin++;
			}
			count++;
		}
		if(xwin > 0) {
			System.out.println("X Win!!!");
		}else if(owin >0) {
			System.out.println("O Win!!!");
		}else {
			System.out.println("Draw!!!");
		}
	}

}