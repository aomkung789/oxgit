import java.util.Scanner;

public class NewXO {
	public static String player = "X";
	public static int roundturn = 0;
        public static String[][] table = { { "_", "_", "_" },
                                           { "_", "_", "_" }, 
                                           { "_", "_", "_" } };
        public static void main(String[] args) {
		showWelcome();		
		while (true) {
                        showTable();
			showTurn();
			inputRowCol();
                        if(checkWin()){
                            break;
                        }
                        switchPlayer();   
		}
		if(checkDraw() == true) {
			showDraw();
		} else {
			showWin();
		}
	}
        
	public static void showWelcome() {
		System.out.println("Welcome to OX Game");
	}  
        
	public static void showTable() {
		for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table.length; j++) {
				System.out.print(table[i][j] + " ");
			}
			System.out.println("");
		}
               System.out.println(""); 
	}
        
	public static void showTurn() {
            System.out.println("Turn "+player);
        }
        
	public static void switchPlayer() {
               if(player.equals("X")){
                   player="O";
               }else{
                   player="X";
               }
	}
        
        public static void roundTurn(){
            roundturn++;
        }
        
	public static void inputRowCol() {
		Scanner kb = new Scanner(System.in);
                while(true){
                    try{
                        System.out.println("Please input Row Col : ");
                        int Row = kb.nextInt() - 1;
                        int Col = kb.nextInt() - 1;
                        if(Col > 3 || Row > 3 || Row<0 || Col < 0){
                            System.out.println("Error : Please input Row Col 1-3");
                            continue;
                        }
                        if(!setTable(Row,Col)){
                            System.out.println("Error : Please input another cell.");
                            continue;
                        }break;
                    }catch(Exception e){
                        System.out.println("Error : Please input Row Col again");
                    }                  
                }
	}
        
        public static boolean setTable(int Row,int Col){
                if(table[Row][Col]!="_"){
                    return false;
                }else{
                    table[Row][Col]=player;
                    return true;
                }
        }

	public static boolean checkWin() {
		if (checkCol() == true) {
			return true;
		} else if (checkRow() == true) {
			return true;
		} else if (checkOblique1() == true) {
			return true;
		} else if (checkOblique2() == true) {
			return true;
		} else if (checkDraw() == true) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean checkCol() {
		for (int i = 0; i < table.length; i++) {
			if (table[0][i].equals(player) && 
					table[1][i].equals(player) && 
					table[2][i].equals(player)) {
				return true;
			}
		}
		return false;
	}

	public static boolean checkRow() {
		for (int i = 0; i < table.length; i++) {
			if (table[i][0].equals(player) && 
					table[i][1].equals(player) && 
					table[i][2].equals(player)) {
				return true;
			}
		}
		return false;
	}

	public static boolean checkOblique1() {
		if (table[0][0].equals(player) && 
				table[1][1].equals(player) && 
				table[2][2].equals(player)) {
			return true;
		}
		return false;
	}

	public static boolean checkOblique2() {
		if (table[0][2].equals(player) && 
				table[1][1].equals(player) && 
				table[2][0].equals(player)) {
			return true;
		}
		return false;
	}

	public static boolean checkDraw() {
		if (roundturn > 8) {
			return true;
		}
		return false;
	}

	public static void showWin() {
		System.out.println("Player " + player + " Win!!");
	}

	public static void showDraw() {
		System.out.println("Draw!!");
	}	
}